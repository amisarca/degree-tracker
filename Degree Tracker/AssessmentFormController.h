//
//  AssessmentFormController.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormActionType.h"
#import "Assessment.h"
#import "Module.h"

@interface AssessmentFormController : UIViewController <UITextFieldDelegate>

/**
 * The text field for the name.
 **/
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

/**
 * The type of the assessment (exam or assignment).
 **/
@property (weak, nonatomic) IBOutlet UISegmentedControl *typeSegmentedControl;

/**
 * The text field for the weight.
 **/
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;

/**
 * The text field for the mark.
 **/
@property (weak, nonatomic) IBOutlet UITextField *markTextField;

/**
 * The text field for the maximum mark.
 **/
@property (weak, nonatomic) IBOutlet UITextField *maxMarkTextField;

/**
 * The type of action performed (ADD or EDIT).
 **/
@property ActionType actionType;

/**
 * The assessment method that is currently dealt with.
 **/
@property (weak) Assessment *assessment;

/**
 * The module that the current assessment method belongs to.
 **/
@property (weak) Module *module;

/**
 * Action called when the form is submitted.
 **/
- (IBAction)savePressed:(id)sender;

@end
