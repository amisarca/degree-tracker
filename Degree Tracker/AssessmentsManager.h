//
//  AssessmentsManager.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 25/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Assessment;

@interface AssessmentsManager : NSObject

/**
 * The path to the database.
 **/
@property (strong) NSString *databasePath;

/**
 * Get the singleton instance responsible for managing assessment methods.
 **/
+ (id)getInstance;

/**
 * Retrieve all assessments for the specified module.
 **/
- (NSMutableArray*)readAllWithModuleId:(NSInteger)mId;

/**
 * Add new assessment for specified module.
 **/
- (NSInteger)addAssessmentWithModuleId:(NSInteger)mId
                                  name:(NSString*)n
                                  type:(NSString*)t
                                weight:(NSInteger)w
                                  mark:(double)m
                               maxMark:(double)maxM;

/**
 * Update assessment.
 **/
- (bool)updateAssessment:(Assessment*)a;

/**
 * Delete assessment.
 **/
- (bool)deleteAssessment:(Assessment*)a;

@end
