//
//  Module.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 13/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "Module.h"
#import "ModulesManager.h"
#import "AssessmentsManager.h"
#import "Assessment.h"

@implementation Module

- (id)initWithYearId:(NSInteger)yId
                code:(NSString *)co
                name:(NSString *)n
             credits:(NSInteger)c
               idNum:(NSInteger)i {
    self = [super init];
    
    if (self) {
        _name = n;
        _yearId = yId;
        _credits = c;
        _idNum = i;
        _code = co;
        
        // Get all the assessment methods for the current module object.
        _assessments = [self retrieveAssessmentsWithModuleId:i];
    }
    return self;
}

- (id) initWithYearId:(NSInteger)yId
                 code:(NSString *)co
                 name:(NSString *)n
              credits:(NSInteger)c {
    self = [super init];
    
    if (self) {
        _name = n;
        _yearId = yId;
        _credits = c;
        _code = co;
    }
    return self;
}

- (NSMutableArray*)retrieveAssessmentsWithModuleId:(NSInteger)mId {
    // Get the singleton responsible for managing assessment methods.
    AssessmentsManager *assessmentsManager = [AssessmentsManager getInstance];
    
    // Retrieve all assessments whose module id is the current module id.
    return [assessmentsManager readAllWithModuleId:mId];
}

- (bool)save {
    // Get the singleton responsible for managing modules.
    ModulesManager *modulesManager = [ModulesManager getInstance];
    
    // Check if the current object has already been inserted into the database.
    if (self.idNum) {
        // Update the current object.
        return [modulesManager updateModule:self];
    } else {
        // Create new object in the database, and get the id.
        NSInteger idNum = [modulesManager addModuleWithYearId:self.yearId
                                                         code:self.code
                                                         name:self.name
                                                      credits:self.credits
                                                  description:self.description];
        if (idNum == -1) {
            return NO;
        } else {
            self.idNum = idNum;
            _assessments = [self retrieveAssessmentsWithModuleId:idNum];
            return YES;
        }
    }
}

- (bool)destroy {
    // Get the singleton responsible for managing modules.
    ModulesManager *modulesManager = [ModulesManager getInstance];
    
    // Delete the current object from the database.
    return [modulesManager deleteModule:self];
}

- (ModuleMarkPair*)markPair {
    if (self.assessments) {
        // Initialize the denominator, and the numerator of the fraction.
        int num = 0, den = 0;
        
        // Iterate over the assignments, and compute the weighted arithmetic mean for the assessment
        // methods.
        for (Assessment *a in self.assessments) {
            if (a.mark != -1) {
                num += (a.mark / a.maxMark) * 100 * a.weight;
                den += a.weight;
            }
        }
        
        if (den != 0) {
            return [[ModuleMarkPair alloc] initWithMark:(double)num/den weight:den];
        } else {
            return [[ModuleMarkPair alloc] initWithMark:-1 weight:0];
        }
    } else {
        return [[ModuleMarkPair alloc] initWithMark:-1 weight:0];
    }
}

- (double)mark {
    return self.markPair.mark;
}

- (bool)fullyMarked {
    return self.markPair.weight == 100;
}

@end
