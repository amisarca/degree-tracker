//
//  Module.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 13/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModuleMarkPair.h"

@interface Module : NSObject

/**
 * The id of the module, as found in the database.
 **/
@property (assign) NSInteger idNum;

/**
 * The id of the year the module belongs to.
 **/
@property (assign) NSInteger yearId;

/**
 * The number of credits for the module.
 **/
@property (assign) NSInteger credits;

/**
 * The code of the module.
 **/
@property (strong) NSString *code;

/**
 * The name of the module.
 **/
@property (strong) NSString *name;

/**
 * The description of the module.
 **/
@property (strong) NSString *description;

/**
 * The array of assessment methods for the module.
 **/
@property (strong) NSMutableArray *assessments;

/**
 * The mark, and the number of marked percentage for the module.
 **/
@property (strong, nonatomic) ModuleMarkPair *markPair;

/**
 * The mark for the module.
 **/
@property (assign, nonatomic) double mark;

/**
 * The indication that the module has been fully marked.
 **/
@property (assign, nonatomic) bool fullyMarked;

/**
 * The constructor for the assessment that takes all the properties but the id.
 **/
- (id)initWithYearId:(NSInteger)yId
                code:(NSString*)co
                name:(NSString*)n
             credits:(NSInteger)c
               idNum:(NSInteger)i;

/**
 * The constructor for the assessment that takes all the properties including the id.
 **/
- (id)initWithYearId:(NSInteger)yId
                code:(NSString*)co
                name:(NSString*)n
             credits:(NSInteger)c;

/**
 * Save the assessment object to the database.
 **/
- (bool)save;

/**
 * Delete the assessment object from the database.
 **/
- (bool)destroy;

@end
