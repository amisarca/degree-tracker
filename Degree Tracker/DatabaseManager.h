//
//  DatabaseManager.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 21/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseManager : NSObject

/**
 * The path to the database.
 **/
@property (strong) NSString *databasePath;

/**
 * The database name.
 **/
@property (strong) NSString *databaseName;

/**
 * Get the singleton instance.
 **/
+ (id)getInstance;

/**
 * Initialise database details.
 **/
- (void)initializeDatabase;

/**
 * Create tables to store years, modules, assessments.
 **/
- (void)createTables;

/**
 * Create table with statement.
 **/
- (void)createTableWithStatement:(char*)stmt;

@end
