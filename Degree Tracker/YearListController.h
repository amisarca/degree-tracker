//
//  YearListController.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 11/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YearsManager.h"

@interface YearListController : UIViewController <UITableViewDelegate, UITableViewDataSource>

/**
 * The length of the course (3 or 4 years).
 **/
@property (assign) NSInteger courseLen;

/**
 * The year manager that reads all the years from the database.
 **/
@property (weak) YearsManager *yearsManager;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 * The segmented control that changes the length of the course.
 **/
@property (weak, nonatomic) IBOutlet UISegmentedControl *courseLength;

/**
 * Action called after the user pressed the segmented control.
 **/
- (IBAction)pressSegment:(id)sender;

@end
