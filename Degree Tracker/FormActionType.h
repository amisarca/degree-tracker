//
//  FormActionType.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 16/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

/**
 * The types of actions for a form.
 **/
typedef enum actionTypes {
    ADD,
    EDIT
} ActionType;
