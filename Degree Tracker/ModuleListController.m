//
//  ModuleListController.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 14/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "ModuleListController.h"
#import "Module.h"
#import "ModuleFormController.h"

@implementation ModuleListController

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.year.modules.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Fill the cell with the name, and the credits of the module.
    static NSString *CellIdentifier = @"moduleCell";
    Module *m = [self.year.modules objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = m.name;
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%d", (int)m.credits];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the module if the user swipes.
        Module *m = [self.year.modules objectAtIndex:indexPath.row];
        [self.year.modules removeObjectAtIndex:indexPath.row];
        [m destroy];
        m = nil;

        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"addModule"]) {
        // Prepare the module form controller for adding a module.
        ModuleFormController *moduleFormController = [segue destinationViewController];
        moduleFormController.actiontype = ADD;
        moduleFormController.year = self.year;

    } else if ([[segue identifier] isEqualToString:@"editModule"]) {
        // Prepare the module form controller for editing a module.
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Module *m = [self.year.modules objectAtIndex:indexPath.row];
        ModuleFormController *moduleFormController = [segue destinationViewController];
        moduleFormController.actiontype = EDIT;
        moduleFormController.year = self.year;
        moduleFormController.module = m;
    }
}

@end
