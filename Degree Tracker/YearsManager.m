//
//  YearsManager.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "YearsManager.h"
#import "DatabaseManager.h"
#import "Year.h"
#import <sqlite3.h>

@implementation YearsManager

#pragma mark Singleton Methods


// retrieve instance for years
+ (id)getInstance {
    // Get the singleton instance and return it.
    static YearsManager *yearsManagerInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        yearsManagerInstance = [[self alloc] init];
    });
    return yearsManagerInstance;
}

- (id)init {
    self = [super init];
    
    // retrieve path to database
    if (self) {
        // Retrieve the database manager singleton, and get the database path.
        DatabaseManager *databaseManager = [DatabaseManager getInstance];
        _databasePath = databaseManager.databasePath;
        _years = [[NSMutableArray alloc] init];
        
        // Retrieve all the years from the database.
        [self readAll];
    }
    
    return self;
}

// retrieve the number of years stored so far
- (NSString*)getNextYear {
    int yearsNumber = self.years.count+1;
    
    // display formatted message 
    switch (yearsNumber) {
        case 1:
            return [NSString stringWithFormat:@"%dst Year", yearsNumber];
            break;
            
        case 2:
            return [NSString stringWithFormat:@"%dnd Year", yearsNumber];
            break;
            
        case 3:
            return [NSString stringWithFormat:@"%drd Year", yearsNumber];
            break;
            
        default:
            return [NSString stringWithFormat:@"%dth Year", yearsNumber];
    }
    
    // consider exceptions in formatting
    if (yearsNumber % 10 == 1 && yearsNumber != 11) {
        return [NSString stringWithFormat:@"%dst Year", yearsNumber];
    } else if (yearsNumber % 10 == 2 && yearsNumber != 12) {
        return [NSString stringWithFormat:@"%dnd Year", yearsNumber];
    } else if (yearsNumber % 10 == 3 && yearsNumber != 13) {
        return [NSString stringWithFormat:@"%drd Year", yearsNumber];
    } else {
        return [NSString stringWithFormat:@"%dth Year", yearsNumber];
    }
}

  // add a new year into the database
- (bool)addYearUntil:(NSInteger)len {
    // Do not add more than 4 years.
    if (self.years.count >= len) {
        return NO;
    }
    
    // insert a new year
    sqlite3 *database;
    NSString *yearName = [self getNextYear];
    
    if (sqlite3_open_v2([self.databasePath UTF8String], &database,
                        SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // make a compiled SQL statement for faster access
        const char *sqlStatement = "INSERT INTO years (name) VALUES (?);";
        sqlite3_stmt *compiledStatement;
        
        // database handle
        int result = sqlite3_prepare_v2(database, sqlStatement, -1,
                                        &compiledStatement, NULL);
        // collect data
        if (result==SQLITE_OK) {
            sqlite3_bind_text(compiledStatement, 1, [yearName UTF8String], -1,
                              SQLITE_TRANSIENT);
            sqlite3_step(compiledStatement);
            // Find the primary key for the item we just inserted.
            NSInteger idNum = sqlite3_last_insert_rowid(database);
            // Now add the same data to the array.
            Year *y = [[Year alloc] initWithName:yearName idNum:idNum];
            
            // Insert at end of array
            [self.years addObject:y];
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result,
                  sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }

    return YES;
}

- (void)readAll {
    sqlite3 *database;
    if (sqlite3_open_v2([self.databasePath UTF8String], &database,
                        SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // Make a compiled SQL statement for faster access.
        const char *sqlStatement = "SELECT * FROM years";
        sqlite3_stmt *compiledStatement;
        
        // database handle
        int result = sqlite3_prepare_v2(database, sqlStatement, -1,
                                        &compiledStatement, NULL);
        
        // collect data
        if (result==SQLITE_OK){
            while (sqlite3_step(compiledStatement)==SQLITE_ROW) {
                // Read the data from the result row.
                NSInteger i = sqlite3_column_int(compiledStatement,0);
                NSString *n = [NSString stringWithUTF8String:
                               (char*)sqlite3_column_text(compiledStatement,
                                                          1)];
                // Create a new year with these details.
                Year *y = [[Year alloc] initWithName:n idNum:i];
                // Insert it into the array.
                [self.years addObject:y];
            }
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result,
                  sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
}

-(void)deleteYearAtIndex:(NSInteger)index {
    // Get the id of the year.
    NSInteger idNum = [[self.years objectAtIndex:index] idNum];
    // Remove the year from the array.
    [self.years removeObjectAtIndex:index];
    
    // Remove the year from the database.
    sqlite3 *database;
    if (sqlite3_open_v2([self.databasePath UTF8String], &database,
                        SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        const char *sqlStatement = "DELETE FROM years WHERE id=?";
        sqlite3_stmt *compiledStatement;
        
        // delete entry with identified id 
        int result = sqlite3_prepare_v2(database, sqlStatement, -1,
                                        &compiledStatement, NULL);
        if (result==SQLITE_OK) {
            sqlite3_bind_int(compiledStatement, 1, idNum);
            sqlite3_step(compiledStatement);
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s",
                  result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
}

@end
