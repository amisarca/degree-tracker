//
//  ModulesManager.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Year.h"

@class Module;

@interface ModulesManager : NSObject

@property (strong) NSString *databasePath;

/**
 * Get the singleton instance responsible for managing modules.
 **/
+ (id)getInstance;

/**
 * Retrieve all modules for the specified year.
 **/
- (NSMutableArray*)readAllWithYearId:(NSInteger)yId;

/**
 * Add module for the specified year.
 **/
- (NSInteger)addModuleWithYearId:(NSInteger)yId
                            code:(NSString*)co
                            name:(NSString*)n
                         credits:(NSInteger)c
                     description:(NSString*)d;

/**
 * Update module.
 **/
- (bool)updateModule:(Module*)m;

/**
 * Delete module.
 **/
- (bool)deleteModule:(Module*)m;

@end
