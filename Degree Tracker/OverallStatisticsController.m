//
//  OverallStatisticsController.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 26/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "OverallStatisticsController.h"
#import "Year.h"

@implementation OverallStatisticsController

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Print the mark for each year on separate lines.
    self.individualYearsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.individualYearsLabel.numberOfLines = 0;
    NSMutableString *yearsDetails = [[NSMutableString alloc] init];
    
    for (Year *y in self.years) {
        [yearsDetails appendString: [NSString stringWithFormat:@"%@: %@", y.name, y.strMark]];
        [yearsDetails appendString:@"\n"];
    }
    self.individualYearsLabel.text = yearsDetails;
    
    // Print the overall mark, and the classification.
    self.overallMark = [Year finalMarkForYears:self.years courseLen:self.courseLen];
    self.overallMarkLabel.text = [Year stringForMark:self.overallMark];
    self.overallDegreeLabel.text = [Year degreeForMark:self.overallMark];
}

- (IBAction)messagePressed:(UIButton *)sender {
    // Decide which service we are using
    NSString *serviceType;
    switch (self.segment.selectedSegmentIndex) {
        case 0:
            serviceType = SLServiceTypeFacebook;
            break;
        case 1:
            serviceType = SLServiceTypeTwitter;
            break;
    }
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:serviceType];
    if ([SLComposeViewController isAvailableForServiceType:serviceType]) {
        [controller setInitialText: [NSString stringWithFormat:@"I got a %@ overall in University", [Year degreeForMark:self.overallMark]]];
        [controller addImage:[UIImage imageNamed:@"trophy.png"]];
        controller.completionHandler=^(SLComposeViewControllerResult result) {
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Cancelled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Posted");
                    break;
            }
        };
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
