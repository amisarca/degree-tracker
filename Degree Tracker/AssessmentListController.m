//
//  AssessmentListController.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "AssessmentListController.h"
#import "Assessment.h"
#import "AssessmentFormController.h"

@implementation AssessmentListController

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.module.assessments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Fill the cell with the name, and the mark of an assessment method.

    static NSString *CellIdentifier = @"assessmentCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Assessment *a = [self.module.assessments objectAtIndex:indexPath.row];
    cell.textLabel.text = a.name;
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%d / %d", (int)a.mark, (int)a.maxMark];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the assessment if the user swipes.
        Assessment *a = [self.module.assessments objectAtIndex:indexPath.row];
        [self.module.assessments removeObjectAtIndex:indexPath.row];
        [a destroy];
        a = nil;
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


#pragma mark - Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"addAssessment"]) {
        // Prepare the assessment form controller for adding an assessment method.
        AssessmentFormController *assessmentFormController = [segue destinationViewController];
        assessmentFormController.actionType = ADD;
        assessmentFormController.module = self.module;
        
    } else if ([[segue identifier] isEqualToString:@"editAssessment"]) {
        // Prepare the assessment form controller for editing an assessment method.
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Assessment *a = [self.module.assessments objectAtIndex:indexPath.row];
        AssessmentFormController *assessmentFormController = [segue destinationViewController];
        assessmentFormController.actionType = EDIT;
        assessmentFormController.module = self.module;
        assessmentFormController.assessment = a;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
