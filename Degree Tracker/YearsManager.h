//
//  YearsManager.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YearsManager : NSObject

/**
 * The path to the database.
 **/
@property (strong) NSString *databasePath;

/**
 * The list of all years.
 **/
@property (strong) NSMutableArray *years;

/**
 * Get the singleton instance.
 **/
+ (id)getInstance;

/**
 * Get all years from the database.
 **/
- (void)readAll;

/**
 * Add a new years, having an upper limit.
 **/
- (bool)addYearUntil:(NSInteger)len;

/**
 * Generate the name of the next year.
 **/
- (NSString*)getNextYear;

/**
 * Delete the year with a given index.
 **/
- (void)deleteYearAtIndex:(NSInteger)index;

@end
