//
//  OverallStatisticsController.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 26/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface OverallStatisticsController : UIViewController

/**
 * The array containing all the years.
 **/
@property (weak) NSMutableArray *years;

/**
 * The course length (3 or 4 years).
 **/
@property (assign) NSInteger courseLen;

/**
 * The overall mark for all years.
 **/
@property (assign) double overallMark;

/**
 * The label all years marks individually.
 **/
@property (weak, nonatomic) IBOutlet UILabel *individualYearsLabel;

/**
 * The label for the overall mark.
 **/
@property (weak, nonatomic) IBOutlet UILabel *overallMarkLabel;

/**
 * The label for the degree classification.
 **/
@property (weak, nonatomic) IBOutlet UILabel *overallDegreeLabel;

/**
 * The segment for choosing social networks.
 **/
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;

/**
 * Post on social networks.
 **/
- (IBAction)messagePressed:(id)sender;

@end
