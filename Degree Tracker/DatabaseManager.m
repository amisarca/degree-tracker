//
//  DatabaseManager.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 21/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "DatabaseManager.h"
#import "sqlite3.h"

@implementation DatabaseManager

#pragma mark Singleton Methods

+ (id)getInstance {
    static DatabaseManager *databaseManagerInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        databaseManagerInstance = [[self alloc] init];
    });
    return databaseManagerInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        // Override point for customization after application launch.
        self.databaseName = @"degree.sql";
        
        // Get path to the database.
        NSArray *documentPaths =
            NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                NSUserDomainMask,
                                                YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        self.databasePath =
            [documentsDir stringByAppendingPathComponent:self.databaseName];
        
        // Initialise the database.
        [self initializeDatabase];
    }
    return self;
}

- (void)createTableWithStatement:(char*)stmt {
    sqlite3 *database;
    if (sqlite3_open_v2([self.databasePath UTF8String], &database, SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // Make a compiled SQL statement for faster access.
        sqlite3_stmt *compiledStatement;
        
        // Database handle.
        int result = sqlite3_prepare_v2(database, stmt, -1, &compiledStatement, NULL);
        
        // Display errors in case of failure in creation.
        if (result==SQLITE_OK){
            int success = sqlite3_step(compiledStatement);
            if (success != SQLITE_DONE) {
                NSLog(@"Failed to create table with error %s",
                      sqlite3_errmsg(database));
            }
            sqlite3_reset(compiledStatement);
        } else {
            NSLog(@"Create database - could not prepare statement: prepare-error #%i: %s", result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
 
}

- (void)createTables {
    
    // Create tables for years, modules and assessments.
    [self createTableWithStatement:"create table if not exists years (id INTEGER PRIMARY KEY, name VARCHAR(100))"];
    [self createTableWithStatement:"create table if not exists modules (id INTEGER PRIMARY KEY, year_id INTEGER, code VARCHAR(100), name VARCHAR(100), credits INTEGER, description TEXT)"];
    [self createTableWithStatement:"create table if not exists assessments (id INTEGER PRIMARY KEY, module_id INTEGER, name VARCHAR(100), type VARCHAR(100), weight INTEGER, mark REAL, max_mark REAL)"];
}

- (void)initializeDatabase {
    // Use a file manager object to find the location of the database.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Check if the database is in the documents folder.
    
    if (![fileManager fileExistsAtPath:self.databasePath]) {
        // Otherwise, get path of sql file in application bundle.
        
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath]
                                         stringByAppendingPathComponent:
                                         self.databaseName];
        
        // Copy database to documents directory.
        
        [fileManager copyItemAtPath:databasePathFromApp
                             toPath:self.databasePath
                              error:nil];
    }
    
    [self createTables];
}

@end
