//
//  YearListController.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 11/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "YearListController.h"
#import "YearViewController.h"
#import "OverallStatisticsController.h"
#import "YearsManager.h"
#import "Year.h"

@implementation YearListController

// Add a new year
- (IBAction)addYear {
    // Get the course length.
    switch (self.courseLength.selectedSegmentIndex) {
        case 0:
            self.courseLen = 3;
            break;
        case 1:
            self.courseLen = 4;
            break;
    }

    // Do not add more years than it is permitted.
    if ([self.yearsManager addYearUntil:self.courseLen]) {
        NSIndexPath *indexPath =
            [NSIndexPath indexPathForRow:self.yearsManager.years.count-1 inSection:0];
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Retrieve the years manager singleton.
    self.yearsManager = [YearsManager getInstance];
    
    // Add the add button.
    UIBarButtonItem *addButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                      target:self
                                                      action:@selector(addYear)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.courseLen = 3;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.yearsManager.years.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Fill the cell with the name of the year.
    
    static NSString *CellIdentifier = @"yearCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Year *y = [self.yearsManager.years objectAtIndex:indexPath.row];
    cell.textLabel.text = y.name;
    return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Remove from array and database.
        [self.yearsManager deleteYearAtIndex:indexPath.row];
        // Remove from table
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showYear"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Year *y = [self.yearsManager.years objectAtIndex:indexPath.row];
        YearViewController *detail = [segue destinationViewController];
        detail.year = y;
    } else if ([[segue identifier] isEqualToString:@"overallStatistics"]) {
        OverallStatisticsController *controller = [segue destinationViewController];
        controller.years = self.yearsManager.years;
        controller.courseLen = self.courseLen;
    }
}

- (IBAction)pressSegment:(id)sender {
    // Warn when moving from 4 years to 3 years.
    switch (self.courseLength.selectedSegmentIndex) {
        case 0:
            if(self.yearsManager.years.count == 4){
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:@"Warning!"
                                      message:@"You are about to delete 4th year data!"
                                      delegate:self
                                      cancelButtonTitle:@"Cancel"
                                      otherButtonTitles:@"OK", nil];
                [alert show];
            }
            break;
        case 1:
            self.courseLen = 4;
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger) buttonIndex {
    
    if (buttonIndex == 1) {
        self.courseLen = 3;
        [self.yearsManager deleteYearAtIndex:self.courseLen];
        [self.tableView reloadData];
    }
    else{
        self.courseLen = 4;
        self.courseLength.selectedSegmentIndex = 1;
    }
}

@end
