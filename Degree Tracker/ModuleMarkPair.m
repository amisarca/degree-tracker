//
//  ModuleMarkPair.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 26/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "ModuleMarkPair.h"

@implementation ModuleMarkPair

- (id)initWithMark:(double)m weight:(int)w {
    self = [super init];
    
    if (self) {
        _mark = m;
        _weight = w;

        // If there is no mark (i.e. the mark has the value -1), then it is marked as empty.
        if (_mark == -1) {
            _empty = YES;
        } else {
            _empty = NO;
        }
    }
    return self;
}

@end
