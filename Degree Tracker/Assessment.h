//
//  Assessment.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Assessment : NSObject

/**
 * The id of the assessment, as found in the database.
 **/
@property (assign) NSInteger idNum;

/**
 * The id of the module the assessment belongs to.
 **/
@property (assign) NSInteger moduleId;

/**
 * The weight (percentage) of the assessment to the final mark for the module.
 **/
@property (assign) NSInteger weight;

/**
 * The mark of the assessment.
 **/
@property (assign) double mark;

/**
 * The maximum mark that can be obtained for the assessment.
 **/
@property (assign) double maxMark;

/**
 * The type of the assessment (exam or assignment).
 **/
@property (strong) NSString *type;

/**
 * The name of the assessment.
 **/
@property (strong) NSString *name;

/**
 * The constructor for the assessment that takes all the properties but the id.
 **/
- (id)initWithModuleId:(NSInteger)mId
                weight:(NSInteger)w
                  mark:(double)m
               maxMark:(double)maxM
                  type:(NSString*)t
                  name:(NSString*)n
                 idNum:(NSInteger)i;

/**
 * The constructor for the assessment that takes all the properties including the id.
 **/
- (id)initWithModuleId:(NSInteger)mId
                weight:(NSInteger)w
                  mark:(double)m
               maxMark:(double)maxM
                  type:(NSString*)t
                  name:(NSString*)n;

/**
 * Save the assessment object to the database.
 **/
- (bool)save;

/**
 * Delete the assessment object from the database.
 **/
- (bool)destroy;

@end
