//
//  ModulesManager.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "ModulesManager.h"
#import "DatabaseManager.h"
#import "Module.h"
#import "sqlite3.h"

@implementation ModulesManager

#pragma mark Singleton Methods

+ (id)getInstance {
    // Get the singleton responsible for managing assessment methods.
    static ModulesManager *modulesManagerInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        modulesManagerInstance = [[self alloc] init];
    });
    return modulesManagerInstance;
}

- (id)init {
    self = [super init];
    
    // Retrieve path to database.
    if (self) {
        DatabaseManager *databaseManager = [DatabaseManager getInstance];
        _databasePath = databaseManager.databasePath;
    }
    
    return self;
}


- (NSInteger)addModuleWithYearId:(NSInteger)yId
                            code:(NSString *)co
                            name:(NSString *)n
                         credits:(NSInteger)c
                     description:(NSString *)d {
    
    // Insert new module to database.
    sqlite3 *database;
    NSInteger idNum = -1;
    
    if (sqlite3_open_v2([self.databasePath UTF8String], &database, SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // Make a compiled SQL statement for faster access.
        const char *sqlStatement = "INSERT INTO modules (year_id, code, name, credits) VALUES (?, ?, ?, ?)";
        sqlite3_stmt *compiledStatement;
        
        // Database handle.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL);
        if (result==SQLITE_OK) {
            sqlite3_bind_int(compiledStatement, 1, yId);
            sqlite3_bind_text(compiledStatement, 2, [co UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement, 3, [n UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(compiledStatement, 4, c);
            sqlite3_step(compiledStatement);
            
            // Find the primary key for the item just inserted in the database.
            idNum = sqlite3_last_insert_rowid(database);
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    
    return idNum;
}

- (bool)updateModule:(Module *)m {
    sqlite3 *database;
    bool returnVal = NO;
    
    if (sqlite3_open_v2([self.databasePath UTF8String], &database, SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // Make a compiled SQL statement for faster access.
        const char *sqlStatement = "UPDATE modules SET year_id=?, code=?, name=?, credits=? WHERE id=?";
        sqlite3_stmt *compiledStatement;
        
        // Database handle.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL);
        if (result==SQLITE_OK){
            // Update fields in table.
            sqlite3_bind_int(compiledStatement, 1, m.yearId);
            sqlite3_bind_text(compiledStatement, 2, [m.code UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement, 3, [m.name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(compiledStatement, 4, m.credits);
            sqlite3_bind_int(compiledStatement, 5, m.idNum);
            
            // Display message upon completion.
            int success = sqlite3_step(compiledStatement);
            if (success == SQLITE_DONE) {
                returnVal = YES;
            } else {
                NSLog(@"Failed to save with error %s",sqlite3_errmsg(database));
            }
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    return returnVal;
}


- (NSMutableArray*)readAllWithYearId:(NSInteger)yId {
    sqlite3 *database;
    
    // Collect all stored modules.
    NSMutableArray *modules = [[NSMutableArray alloc] init];

    if (sqlite3_open_v2([self.databasePath UTF8String], &database,
                        SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // Make a compiled SQL statement for faster access.
        const char *sqlStatement = "SELECT * FROM modules where year_id=?";
        sqlite3_stmt *compiledStatement;
        
        // Database handle.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1,
                                        &compiledStatement, NULL);
        
        // Collect data.
        if (result==SQLITE_OK) {
            sqlite3_bind_int(compiledStatement, 1, yId);
            while (sqlite3_step(compiledStatement)==SQLITE_ROW) {
                // Read the data from the result row.
                NSInteger i = sqlite3_column_int(compiledStatement, 0);
                NSInteger yId = sqlite3_column_int(compiledStatement, 1);
                NSString *co = [NSString stringWithUTF8String:
                               (char*)sqlite3_column_text(compiledStatement, 2)];
                NSString *n = [NSString stringWithUTF8String:
                               (char*)sqlite3_column_text(compiledStatement, 3)];
                NSInteger c = sqlite3_column_int(compiledStatement, 4);
                
                // Create a new contact with these details.
                Module *m = [[Module alloc] initWithYearId:yId code:co name:n credits:c idNum:i];
                // Insert it into the array.
                [modules addObject:m];
            }
        } else {
            // Display error upon failure.
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result,
                  sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    return modules;
}


- (bool)deleteModule:(Module *)m {
    NSInteger idNum = m.idNum;
    bool returnVal = NO;

    // Remove from database.
    sqlite3 *database;
    if (sqlite3_open_v2([self.databasePath UTF8String], &database,
                        SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        const char *sqlStatement = "DELETE FROM modules WHERE id=?";
        sqlite3_stmt *compiledStatement;
        
        // Delete the module with identified id.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1,
                                        &compiledStatement, NULL);
        if (result==SQLITE_OK) {
            sqlite3_bind_int(compiledStatement, 1, idNum);
            sqlite3_step(compiledStatement);
            returnVal = YES;
            m = nil;
        } else {
            // Display error upon failure.
            NSLog(@"Could not prepare statement: prepare-error #%i: %s",
                  result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    return returnVal;
}
@end
