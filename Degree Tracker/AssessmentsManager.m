//
//  AssessmentsManager.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 25/11/2013.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "AssessmentsManager.h"
#import "DatabaseManager.h"
#import "sqlite3.h"
#import "Assessment.h"

@implementation AssessmentsManager

#pragma mark Singleton Methods

+ (id)getInstance {
    // Get the singleton instance responsible for managing assessment methods.
    static AssessmentsManager *assessmentsManagerInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        assessmentsManagerInstance = [[self alloc] init];
    });
    return assessmentsManagerInstance;
}

- (id)init {
    self = [super init];
    
    // Retrieve path to database.
    if (self) {
        DatabaseManager *databaseManager = [DatabaseManager getInstance];
        _databasePath = databaseManager.databasePath;
    }
    
    return self;
}

- (NSArray*)readAllWithModuleId:(NSInteger)mId {
    sqlite3 *database;
    
    // Collect all stored assessments.
    NSMutableArray *assessments = [[NSMutableArray alloc] init];
    
    if (sqlite3_open_v2([self.databasePath UTF8String], &database,
                        SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // Make a compiled SQL statement for faster access.
        const char *sqlStatement = "SELECT * FROM assessments where module_id=?";
        sqlite3_stmt *compiledStatement;
        
        // Database handle.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1,
                                        &compiledStatement, NULL);
        // Collect data.
        if (result==SQLITE_OK) {
            sqlite3_bind_int(compiledStatement, 1, mId);
            while (sqlite3_step(compiledStatement)==SQLITE_ROW) {
                // Read the data from the result row.
                NSInteger i = sqlite3_column_int(compiledStatement, 0); 
                NSInteger mId = sqlite3_column_int(compiledStatement, 1);
                NSString *n = [NSString stringWithUTF8String: 
                               (char*)sqlite3_column_text(compiledStatement,
                                                          2)];
                NSString *t = [NSString stringWithUTF8String:
                               (char*)sqlite3_column_text(compiledStatement,
                                                          3)];
                NSInteger w = sqlite3_column_int(compiledStatement, 4);
                double m = sqlite3_column_double(compiledStatement, 5);
                double maxM = sqlite3_column_double(compiledStatement, 6);
                
                // Create a new assessment method with these details.
                Assessment *a = [[Assessment alloc] initWithModuleId:mId
                                                              weight:w
                                                                mark:m
                                                             maxMark:maxM
                                                                type:t
                                                                name:n
                                                               idNum:i];
                // Insert it into the array.
                [assessments addObject:a];
            }
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result,
                  sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    return assessments;
}

- (NSInteger)addAssessmentWithModuleId:(NSInteger)mId
                                  name:(NSString *)n
                                  type:(NSString *)t
                                weight:(NSInteger)w
                                  mark:(double)m
                               maxMark:(double)maxM {
    // Insert a new assessment method into the database.
    sqlite3 *database;
    NSInteger idNum = -1;
    
    if (sqlite3_open_v2([self.databasePath UTF8String], &database, SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        const char *sqlStatement = "INSERT INTO assessments (module_id, name, type, weight, mark, max_mark) VALUES (?, ?, ?, ?, ?, ?)";
        sqlite3_stmt *compiledStatement;
        
        // Database handle.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL);
        if (result==SQLITE_OK) {
            // Collect data.
            sqlite3_bind_int(compiledStatement, 1, mId);
            sqlite3_bind_text(compiledStatement, 2, [n UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement, 3, [t UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(compiledStatement, 4, w);
            sqlite3_bind_double(compiledStatement, 5, m);
            sqlite3_bind_double(compiledStatement, 6, maxM);
            sqlite3_step(compiledStatement);
            
            // Find the primary key for the item we just inserted.
            idNum = sqlite3_last_insert_rowid(database);
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    
    return idNum;
}


- (bool)updateAssessment:(Assessment *)a {
    sqlite3 *database;
    bool returnVal = NO;
    
    if (sqlite3_open_v2([self.databasePath UTF8String], &database, SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        // Make a compiled SQL statement for faster access.
        const char *sqlStatement = "UPDATE assessments SET module_id=?, name=?, type=?, weight=?, mark=?, max_mark=? WHERE id=?";
        sqlite3_stmt *compiledStatement;
        
        // Database handle.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL);
        if (result==SQLITE_OK){
            // Update fields in table.
            sqlite3_bind_int(compiledStatement, 1, a.moduleId);
            sqlite3_bind_text(compiledStatement, 2, [a.name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement, 3, [a.type UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(compiledStatement, 4, a.weight);
            sqlite3_bind_double(compiledStatement, 5, a.mark);
            sqlite3_bind_double(compiledStatement, 6, a.maxMark);
            sqlite3_bind_int(compiledStatement, 7, a.idNum);
            
            int success = sqlite3_step(compiledStatement);
            if (success == SQLITE_DONE) {
                returnVal = YES;
            } else {
                NSLog(@"Failed to save with error %s",sqlite3_errmsg(database));
            }
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s", result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    return returnVal;
}

- (bool)deleteAssessment:(Assessment *)a {
    NSInteger idNum = a.idNum;
    bool returnVal = NO;
    
    // Remove from database.
    sqlite3 *database;
    if (sqlite3_open_v2([self.databasePath UTF8String], &database,
                        SQLITE_OPEN_READWRITE, NULL)==SQLITE_OK) {
        
        const char *sqlStatement = "DELETE FROM assessments WHERE id=?";
        sqlite3_stmt *compiledStatement;
        
        // Delete the assessment method with the identified id.
        int result = sqlite3_prepare_v2(database, sqlStatement, -1,
                                        &compiledStatement, NULL);
        if (result==SQLITE_OK) {
            sqlite3_bind_int(compiledStatement, 1, idNum);
            sqlite3_step(compiledStatement);
            returnVal = YES;
            a = nil;
        } else {
            NSLog(@"Could not prepare statement: prepare-error #%i: %s",
                  result, sqlite3_errmsg(database));
        }
        // Release the compiled statement.
        sqlite3_finalize(compiledStatement);
        // Close the database.
        sqlite3_close(database);
    } else {
        NSLog(@"Could not open database");
    }
    return returnVal;
}


@end
