//
//  DetailViewController.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 09/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "YearViewController.h"
#import "ModuleListController.h"

@implementation YearViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showMark];
    self.detailDescriptionLabel.text = self.year.name;
}

- (void)showMark {
    // Show the mark (as number, and the classification).
    self.markLabel.text = self.year.strMark;
    self.degreeLabel.text = self.year.degree;
    
    // Show a message stating if the year has been fully marked.
    if (self.year.fullyMarked) {
        self.commentLabel.text = @"The mark is final";
    } else {
        self.commentLabel.text = @"The mark is not final";
    }
}

- (IBAction)messagePressed:(UIButton *)sender {
    // Decide which service we are using.
    NSString *serviceType;
    switch (self.segment.selectedSegmentIndex) {
        case 0:
            serviceType = SLServiceTypeFacebook;
            break;
        case 1:
            serviceType = SLServiceTypeTwitter;
            break;
    }
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:serviceType];
    if ([SLComposeViewController isAvailableForServiceType:serviceType]) {
        [controller setInitialText: [NSString stringWithFormat:@"I got a %@ in my %@", self.year.degree, self.year.name]];
        [controller addImage:[UIImage imageNamed:@"trophy.png"]];
        controller.completionHandler=^(SLComposeViewControllerResult result) {
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Cancelled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Posted");
                    break;
            }};
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showModules"]) {
        ModuleListController *detail = [segue destinationViewController];
        detail.year = self.year;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
