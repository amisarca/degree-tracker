//
//  ModuleFormController.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 14/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "ModuleFormController.h"
#import "Module.h"
#import "AssessmentListController.h"

@implementation ModuleFormController

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    [textField resignFirstResponder];
    [self saveModule];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Lift the screen so that the text field is visible when the keyboard is on.
    [self liftTextField:textField up:YES center:textField.center];
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    // Move the screen back after the keyboard disappears.
    [self liftTextField:textField up:NO center:textField.center];
}

// Lift the screen so that the text field is visible when the keyboard is on.
- (void)liftTextField:(UITextField*)textField up:(BOOL)up center:(CGPoint)center {
    const float movementDistance = 20;
    const float movementDuration = 0.3f;
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.creditsTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    if (self.actiontype == EDIT) {
        self.nameTextField.text = self.module.name;
        self.codeTextField.text = self.module.code;
        self.creditsTextField.text = [NSString stringWithFormat:@"%d", self.module.credits];
    } else {
        self.assessmentsButton.hidden = TRUE;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showMark];
}

// Show the mark of the module, if the action is edit.
- (void)showMark {
    if (self.actiontype == EDIT) {
        ModuleMarkPair *moduleMark = self.module.markPair;
        if (moduleMark.empty) {
            self.markLabel.text = @"N/A";
        } else {
            self.markLabel.text = [NSString stringWithFormat:@"%.2f%%", moduleMark.mark];
        }
        self.percentageLabel.text = [NSString stringWithFormat:@"%d%% marked)", moduleMark.weight];
        moduleMark = nil;
    } else {
        self.markView.hidden = TRUE;
    }
}

// If you touch outside the keyboard, then hide the keyboard.
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)saveModule {
     // If the action is add, then create a new module, otherwise the current module is the module associated to the controller.
    Module *module;
    if (self.actiontype == ADD) {
        module = [[Module alloc] init];
        [self.year.modules addObject:module];
    } else {
        module = self.module;
    }
    
    // Set the properties of the module to the ones from the form.
    module.name = self.nameTextField.text;
    module.code = self.codeTextField.text;
    module.credits = [self.creditsTextField.text integerValue];
    module.yearId = self.year.idNum;
    [module save];
    [self.navigationController popViewControllerAnimated:TRUE];
}

// Check if the required fields have been filled.
- (IBAction)savePressed:(id)sender {
    if([self.nameTextField.text length] == 0 || [self.codeTextField.text length] == 0){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Warning!"
                              message:@"Please complete required fields!"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles: nil];
        [alert show];
    }
    else{
        [self saveModule];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showAssessments"]) {
        AssessmentListController *assessmentListController = [segue destinationViewController];
        assessmentListController.module = self.module;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
