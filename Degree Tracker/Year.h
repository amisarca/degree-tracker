//
//  Year.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 10/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Year : NSObject

/**
 * The id of the year, as found in the database.
 **/
@property (assign) NSInteger idNum;

/**
 * The name of the year.
 **/
@property (strong) NSString *name;

/**
 * The array of modules for the year.
 **/
@property (strong) NSMutableArray *modules;

/**
 * The mark for the year.
 **/
@property (assign, nonatomic) double mark;

/**
 * The indication that the year has been fully marked.
 **/
@property (assign, nonatomic) bool fullyMarked;

/**
 * The classification degree for the year.
 **/
@property (strong, nonatomic) NSString *degree;

/**
 * The mark for the year, in printable format.
 **/
@property (strong, nonatomic) NSString *strMark;

/**
 * The constructor for the assessment that takes all the properties including the id.
 **/
- (id)initWithName:(NSString*)n idNum:(NSInteger)i;

/**
 * Method that returns the classified degree for a mark.
 **/
+ (NSString*)degreeForMark:(double)mark;

/**
 * Method that returns the printable format of a mark.
 **/
+ (NSString*)stringForMark:(double)mark;

/**
 * Compute the final mark of all years.
 **/
+ (double)finalMarkForYears:(NSMutableArray*)years courseLen:(NSInteger)courseLen;

@end
