//
//  ModuleMarkPair.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 26/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModuleMarkPair : NSObject

/**
 * The mark for the pair.
 **/
@property (assign) double mark;

/**
 * The weight for the pair.
 **/
@property (assign) int weight;

/**
 * Boolean that indicates that the pair contains no mark.
 **/
@property (assign) bool empty;

/**
 * Default constructor for the pair, having both parameters.
 **/
- (id)initWithMark:(double)m weight:(int)w;

@end
