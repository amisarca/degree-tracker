//
//  AssessmentListController.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Module.h"

@interface AssessmentListController : UITableViewController

/**
 * The module that the current assessment method belongs to.
 **/
@property (weak) Module *module;

@end
