//
//  ModuleFormController.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 14/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormActionType.h"
#import "Year.h"
#import "Module.h"
#import "ModulesManager.h"

@interface ModuleFormController : UIViewController <UITextFieldDelegate>

/**
 * The text field for the name.
 **/
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

/**
 * The text field for the credits.
 **/
@property (weak, nonatomic) IBOutlet UITextField *creditsTextField;

/**
 * The text field for the code.
 **/
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;

/**
 * The view that contains all the details for marks.
 **/
@property (weak, nonatomic) IBOutlet UIView *markView;

/**
 * The label that contains the mark.
 **/
@property (weak, nonatomic) IBOutlet UILabel *markLabel;

/**
 * The label that contains the marked percentage for the module.
 **/
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;

/**
 * The button that redirects to all assessments.
 **/
@property (weak, nonatomic) IBOutlet UIButton *assessmentsButton;

/**
 * The year that the current module method belongs to.
 **/
@property (weak) Year *year;

/**
 * The module that is currently dealt with.
 **/
@property (weak) Module *module;

/**
 * The type of action performed (ADD or EDIT).
 **/
@property (assign) ActionType actiontype;

/**
 * Action called when the form is submitted.
 **/
- (IBAction)savePressed:(id)sender;

@end
