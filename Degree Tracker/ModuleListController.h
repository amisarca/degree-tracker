//
//  ModuleListController.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 14/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YearsManager.h"
#import "Year.h"


@interface ModuleListController : UITableViewController

/**
 * The year that the current modules belongs to.
 **/
@property (weak) Year *year;

@end
