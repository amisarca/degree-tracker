//
//  DetailViewController.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 09/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "Year.h"

@interface YearViewController : UIViewController

/**
 * The year that is currently dealt with.
 **/
@property (weak) Year *year;

/**
 * The label for the mark.
 **/
@property (weak, nonatomic) IBOutlet UILabel *markLabel;

/**
 * The label for the degree.
 **/
@property (weak, nonatomic) IBOutlet UILabel *degreeLabel;

/**
 * The label for the comment (if the year is fully marked or not).
 **/
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

/**
 * The label for the detail description for the year.
 **/
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

/**
 * The segment for choosing social networks.
 **/
@property (weak) IBOutlet UISegmentedControl *segment;

/**
 * Post on social networks.
 **/
- (IBAction)messagePressed:(UIButton *)sender;

@end
