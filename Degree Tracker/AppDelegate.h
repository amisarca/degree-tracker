//
//  AppDelegate.h
//  Degree Tracker
//
//  Created by Andrei Misarca on 09/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
