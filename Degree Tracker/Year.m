//
//  Year.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 10/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "Year.h"
#import "ModulesManager.h"
#import "Module.h"

@implementation Year

- (id)initWithName:(NSString *)n idNum:(NSInteger)i {
    self = [super init];
    
    if (self) {
        _name = n;
        _idNum = i;

        // Get the singleton responsible for managing modules.
        ModulesManager *modulesManager = [ModulesManager getInstance];
        // Find all modules whose year id is the current year's id.
        _modules = [modulesManager readAllWithYearId:i];
    }
    return self;
}

- (double)mark {
    if (self.modules) {
        // Initialize the denominator, and the numerator of the fraction.
        int num = 0, den = 0;
        
        // Iterate over the modules, and compute the weighted arithmetic mean for the modules
        for (Module *m in self.modules) {
            if (m.mark != -1) {
                num += m.mark * m.credits;
                den += m.credits;
            }
        }
        
        if (den != 0) {
            return (double)num/den;
        } else {
            return -1;
        }
    } else {
        return -1;
    }
}

- (bool)fullyMarked {
    if (self.modules) {
        bool result = TRUE;
        
        // Iterate over the modules, and check if all of them have been fully marked.
        for (Module *m in self.modules) {
            result &= m.fullyMarked;
        }
        return result;
    } else {
        return FALSE;
    }
}

- (NSString*)degree {
    return [[self class] degreeForMark:self.mark];
}

+ (NSString*)degreeForMark:(double)mark {
    
    if (mark == -1) {
        return @"N/A";
    }
    
    if (mark >= 70) {
        return @"First Class (1)";
    } else if (mark >= 60) {
        return @"Upper Second (2.1)";
    } else if (mark >= 50) {
        return @"Lower Second (2.2)";
    } else if (mark >= 40) {
        return @"Third (3)";
    } else {
        return @"Marginal Fail";
    }
}

+ (NSString*)stringForMark:(double)mark {
    if (mark == -1) {
        return @"N/A";
    } else {
        return [NSString stringWithFormat:@"%.2f%%", mark];
    }
}

+ (double)finalMarkForYears:(NSMutableArray *)years courseLen:(NSInteger)courseLen {
    double den = 0, num = 0;

    // Iterate over the years, and compute the weighted arithmetic mean of the marks, based on the
    // existent formula for calculating the final mark.
    for (int i = 0; i < years.count; i++) {
        Year *y = years[i];
        
        switch (i) {
            case 1:
                if (courseLen == 3 && y.mark != -1) {
                    num += y.mark * 33;
                    den += 33;
                } else if (courseLen == 4 && y.mark != -1) {
                    num += y.mark * 20;
                    den += 20;
                }
                break;
            case 2:
                if (courseLen == 3 && y.mark != -1) {
                    num += y.mark * 67;
                    den += 67;
                } else if (courseLen == 4 && y.mark != -1) {
                    num += y.mark * 40;
                    den += 40;
                }
                break;
            case 3:
                if (courseLen == 4 && y.mark != -1) {
                    num += y.mark * 40;
                    den += 40;
                }
                break;
            default:
                break;
        }
    }
    
    if (den != 0) {
        return num/den;
    } else {
        return -1;
    }
}

- (NSString*)strMark {
    return [[self class] stringForMark:self.mark];
}

@end
