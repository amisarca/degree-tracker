//
//  Assessment.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "Assessment.h"
#import "AssessmentsManager.h"

@implementation Assessment

- (id)initWithModuleId:(NSInteger)mId
                weight:(NSInteger)w
                  mark:(double)m
               maxMark:(double)maxM
                  type:(NSString *)t
                  name:(NSString *)n {
    self = [super init];
    
    if (self) {
        _moduleId = mId;
        _weight = w;
        _mark = m;
        _maxMark = maxM;
        _type = t;
        _name = n;
    }
    return self;
}

- (id)initWithModuleId:(NSInteger)mId
                weight:(NSInteger)w
                  mark:(double)m
               maxMark:(double)maxM
                  type:(NSString *)t
                  name:(NSString *)n
                 idNum:(NSInteger)i {
    self = [super init];
    
    if (self) {
        _moduleId = mId;
        _weight = w;
        _mark = m;
        _maxMark = maxM;
        _type = t;
        _name = n;
        _idNum = i;
    }
    return self;
}

- (bool)save {
    // Get the singleton responsible for managing assessment methods.
    AssessmentsManager *assessmentsManager = [AssessmentsManager getInstance];
    
    // Check if the current object has already been inserted into the database.
    if (self.idNum) {
        // Update the current object.
        return [assessmentsManager updateAssessment:self];
    } else {
        // Create new object in the database, and get the id.
        NSInteger idNum = [assessmentsManager addAssessmentWithModuleId:self.moduleId
                                                                   name:self.name
                                                                   type:self.type
                                                                 weight:self.weight
                                                                   mark:self.mark
                                                                maxMark:self.maxMark];
        if (idNum == -1) {
            return NO;
        } else {
            self.idNum = idNum;
            return YES;
        }
    }
}

- (bool)destroy {
    // Get the singleton responsible for managing assessment methods.
    AssessmentsManager *assessmentsManager = [AssessmentsManager getInstance];
    
    // Delete the current object from the database.
    return [assessmentsManager deleteAssessment:self];
}

@end
