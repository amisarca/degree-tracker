//
//  AssessmentFormController.m
//  Degree Tracker
//
//  Created by Andrei Misarca on 22/01/2014.
//  Copyright (c) 2014 University of Sheffield. All rights reserved.
//

#import "AssessmentFormController.h"

#define trim(s) [s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

@implementation AssessmentFormController

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.maxMarkTextField || textField == self.markTextField) {
        // Lift the screen so that the text field is visible when the keyboard is on.
        [self liftTextField:textField up:YES movementDistance:70];
    } else if (textField == self.weightTextField) {
        // Lift the screen so that the text field is visible when the keyboard is on.
        [self liftTextField:textField up:YES movementDistance:150];
    }

}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.maxMarkTextField || textField == self.markTextField) {
        // Move the screen back after the keyboard disappears.
        [self liftTextField:textField up:NO movementDistance:70];
    } else if (textField == self.weightTextField) {
        // Move the screen back after the keyboard disappears.
        [self liftTextField:textField up:NO movementDistance:150];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

// Lift the screen so that the text field is visible when the keyboard is on.
- (void)liftTextField:(UITextField*)textField up:(BOOL)up movementDistance:(float)d {
    const float movementDuration = 0.3f;
    
    int movement = (up ? -d : d);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

// If you touch outside the keyboard, then hide the keyboard.
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set the keyboard type for the numeric fields.
    self.weightTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.markTextField.keyboardType = UIKeyboardTypeDecimalPad;
    self.maxMarkTextField.keyboardType = UIKeyboardTypeDecimalPad;
    
    // If the action is edit type, then populate the form with the current details.
    if (self.actionType == EDIT) {
        self.nameTextField.text = self.assessment.name;
        if (self.assessment.maxMark != -1) {
            self.maxMarkTextField.text = [NSString stringWithFormat:@"%.2f", self.assessment.maxMark];
        }
        if (self.assessment.mark != -1) {
            self.markTextField.text = [NSString stringWithFormat:@"%.2f", self.assessment.mark];
        }
        self.weightTextField.text = [NSString stringWithFormat:@"%d", self.assessment.weight];
        
        if ([self.assessment.type isEqual: @"Exam"]) {
            self.typeSegmentedControl.selectedSegmentIndex = 0;
        } else {
            self.typeSegmentedControl.selectedSegmentIndex = 1;
        }
    }
}

- (void)saveAssessment {
    // If the action is add, then create a new assessment method, otherwise the current assessment is the assessment associated to the controller.
    Assessment *assessment;
    if (self.actionType == ADD) {
        assessment = [[Assessment alloc] init];
        [self.module.assessments addObject:assessment];
    } else {
        assessment = self.assessment;
    }
    
    assessment.name = self.nameTextField.text;
    
    // If the maximum mark field is not empty then parse it, and assign it to the assessment, otherwise set both the mark, and the maximum mark to -1.
    if ([trim(self.maxMarkTextField.text) length]) {
        assessment.maxMark = [self.maxMarkTextField.text floatValue];
    } else {
        assessment.mark = -1;
        assessment.maxMark = -1;
    }

    // If both the maximum mark field, and the mark field are not empty, then parse the mark field, and assign it to the assessment, otherwise set it to -1.
    if ([trim(self.markTextField.text) length] && [trim(self.maxMarkTextField.text) length]) {
        assessment.mark = [self.markTextField.text floatValue];
        if (assessment.mark > assessment.maxMark) {
            assessment.mark = assessment.maxMark;
        }
    } else {
        assessment.mark = -1;
    }

    // Parse the weight, and assign it to the assessment.
    assessment.weight = [self.weightTextField.text integerValue];
    assessment.moduleId = self.module.idNum;
    
    // Get the type of assessment form the segmented control.
    switch (self.typeSegmentedControl.selectedSegmentIndex) {
        case 0:
            assessment.type = @"Exam";
            break;
        default:
            assessment.type = @"Assignment";
    }

    // Save the assessment.
    [assessment save];
    [self.navigationController popViewControllerAnimated:TRUE];
}

// Check if the required fields have been filled.
- (IBAction)savePressed:(id)sender {
    if([self.nameTextField.text length] == 0 || [self.weightTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Warning!"
                              message:@"Please complete required fields!"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles: nil];
        [alert show];
    }
    else {
        [self saveAssessment];
    }
}
@end
